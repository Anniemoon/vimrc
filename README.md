#vimrc

Annie's vimrc (for Ruby on Rails) 
@author: Annie Dong

* git clone https://git.oschina.net/Anniemoon/vimrc.git ~/.myvim

* cd ~/.myvim && cp .vimrc ~/.vimrc

* use vundle manage plugin https://github.com/VundleVim/Vundle.vim
  1. step1: git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  2. step2: intall plugin: :PluginInstall